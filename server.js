const http = require("http");
const fs = require("fs");
const qs = require("querystring");
const db = require("./data/data.json");

var logged = "false";

const server = http.createServer((req, res) => {
	if (req.url === "/styles/styles.css") {
		res.statusCode = 200;
		res.setHeader("Content-Type", "text/css");
		fs.readFile("styles/styles.css", function(error, data) {
			if (error) {
				res.writeHead(404);
				res.write("Error: File not found.");
			} else {
				res.write(data);
			}

			res.end();
		});
	}

	if (req.url === "/") {
		res.statusCode = 200;
		res.setHeader("Content-Type", "text/html");
		fs.readFile("pages/landing.html", function(error, data) {
			if (error) {
				res.writeHead(404);
				res.write("Error: File not found.");
			} else {
				res.write(data);
			}

			res.end();
		});
	}

	if (req.url === "/home") {
		res.statusCode = 200;
		res.setHeader("Content-Type", "text/html");
		fs.readFile("./pages/home.html", (error, data) => {
			if (error) {
				res.writeHead(404);
				res.write("Error: File not found.");
			} else {
				res.write(data);
			}

			res.end();
		});
	}

	if (req.url === "/" && logged === "true") {
		res.statusCode = 200;
		res.writeHead(301, { Location: "/home" });
		res.end();
	}

	if (req.url === "/wrong_info") {
		res.statusCode = 200;
		res.setHeader("Content-Type", "text/html");
		fs.readFile("./pages/wronginfo.html", (error, data) => {
			if (error) {
				res.writeHead(404);
				res.write("Error: File not found.");
			} else {
				res.write(data);
			}

			res.end();
		});
	}

	if (req.url === "/log-out") {
		logged = "false";
		res.statusCode = 200;
		res.setHeader("Content-Type", "text/html");
		fs.readFile("pages/logout.html", function(error, data) {
			if (error) {
				res.writeHead(404);
				res.write("Error: File not found.");
			} else {
				res.write(data);
			}

			res.end();
		});
	}

	if (req.url === "/about") {
		res.statusCode = 200;
		res.setHeader("Content-Type", "text/html");
		fs.readFile("pages/about.html", (error, data) => {
			if (error) {
				res.writeHead(404);
				res.write("Error: File not found.");
			} else {
				res.write(data);
			}

			res.end();
		});
	}

	if (req.url === "/news") {
		res.statusCode = 200;
		res.setHeader("Content-Type", "text/html");
		fs.readFile("pages/news.html", (error, data) => {
			if (error) {
				res.writeHead(404);
				res.write("Error: File not found.");
			} else {
				res.write(data);
			}

			res.end();
		});
	}

	if (req.url === "/members" && req.method === "POST") {
		res.statusCode = 200;
		//res.setHeader("Content-Type", "text/json");
		var body = "";

		req.on("data", (data) => {
			body = body + data;
		});

		req.on("end", () => {
			var post = qs.parse(body);
			db.forEach(i => {
				if (post.username === i.username && post.password === i.password) {
					logged = "true";
				}
			});

			if (logged === "true") {
				res.writeHead(301, { Location: "/home" });
			} else {
				res.writeHead(301, { Location: "/wrong_info" });
			}

			res.end();
		});
	}

});

const port = process.env.port || 9000

server.listen(port, () => console.log(`Server listening on port ${port}`));
